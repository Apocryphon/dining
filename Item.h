//
//  Item.h
//  Dining
//
//  Created by Richard Yeh on 2/19/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import <CoreData/CoreData.h>

@class Menu;

@interface Item :  NSManagedObject  
{
}

@property (nonatomic, retain) NSString * price;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSSet * menu;

@end


@interface Item (CoreDataGeneratedAccessors)
- (void)addMenuObject:(Menu *)value;
- (void)removeMenuObject:(Menu *)value;
- (void)addMenu:(NSSet *)value;
- (void)removeMenu:(NSSet *)value;

@end

