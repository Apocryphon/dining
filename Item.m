// 
//  Item.m
//  Dining
//
//  Created by Richard Yeh on 2/19/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import "Item.h"


@implementation Item 

@dynamic price;
@dynamic name;
@dynamic desc;
@dynamic menu;

@end
