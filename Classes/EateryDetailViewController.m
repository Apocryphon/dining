//
//  EateryDetailViewController.m
//  Dining
//
//  Created by Richard Yeh on 12/3/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import "EateryDetailViewController.h"
#import "MapViewController.h"
#import "MenuDetailViewController.h"
#import "Times.h"
#import "HoursRange.h"

@implementation EateryDetailViewController

@synthesize eatHere;
@synthesize cohoSet;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
		
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	
	self.navigationItem.title = eatHere.name;
	
	self.cohoSet = [NSSet setWithObjects:@"Chopstixx",@"Ciao",@"Cooks",@"Croutons",@"Fickle Pickle Deli",
					@"TexMex Grill",@"Swirlz Bakery", nil];

	
	/* what was this for?
	 
	 NSArray *nameslist = [eatHere objectForKey:@"Names"];	
	 NSPredicate *pred = [NSPredicate predicateWithFormat:@"%K == %@", @"Names", @"Carl's Jr."];
	 NSArray *filteredArray = [nameslist filteredArrayUsingPredicate:pred];
	 
	 for (NSDictionary *dict in filteredArray) {
	 NSLog(@"Name = %@", [dict valueForKey:@"Name"]);
	 }

	 */
	
}

/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 4;		// SUSCEPTIBLE TO CHANGE
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.

	if (section == 0)
		return 1;
	else
		return 1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
									   reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
	if (indexPath.section == 0)   //Infobox
	{
		cell.textLabel.text = [NSString stringWithFormat:@"Address: %@\n", eatHere.address];

		if (eatHere.phone != @"") {
			cell.textLabel.text= [cell.textLabel.text stringByAppendingString:@"Phone: "];
			cell.textLabel.text= [cell.textLabel.text stringByAppendingString:eatHere.phone];
			cell.textLabel.text= [cell.textLabel.text stringByAppendingString:@"\n"];
		}

		if (eatHere.website != @"") {
			cell.textLabel.text= [cell.textLabel.text stringByAppendingString:@"Website: "];
			cell.textLabel.text= [cell.textLabel.text stringByAppendingString:eatHere.website];
			cell.textLabel.text= [cell.textLabel.text stringByAppendingString:@"\n"];
		}
		
		cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
		cell.textLabel.numberOfLines = 20; //can arbitrarily increase this because sizetofit will shrink it
		[cell.textLabel sizeToFit]; //shrinks down unused lines for eateries with less info
	}

	else if (indexPath.section == 1)  
	{
		cell.textLabel.text = [[NSString alloc] initWithString:@"Locate on Map"];
		cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
		cell.textLabel.numberOfLines = 0;
		[cell.textLabel sizeToFit];
		
	}

	else if (indexPath.section == 2)  
	{
		if([cohoSet containsObject:eatHere.name]) {
			cell.textLabel.text = [[NSString alloc] initWithString:@"Open Menu"];
			cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
			cell.textLabel.numberOfLines = 0;
			[cell.textLabel sizeToFit];
		}
		else
		{
			cell.textLabel.text = [[NSString alloc] initWithString:@"Check out the Menu"];
			cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
			cell.textLabel.numberOfLines = 0;
			[cell.textLabel sizeToFit];
		}
	}
	
	else if (indexPath.section == 3)
	{
		NSArray *weekDays = [[NSArray alloc] initWithObjects: @"Monday", @"Tuesday", @"Wednesday", 
							 @"Thursday", @"Friday", @"Saturday", @"Sunday", nil];

		if (eatHere.times != nil)
		{
			cell.textLabel.text= [[NSString alloc] initWithString:@"Opening hours"];
			cell.textLabel.text= [cell.textLabel.text stringByAppendingString:@"\n"];
			
			for (int i=0; i<7; i++) 
			{
				cell.textLabel.text= [cell.textLabel.text stringByAppendingString:@"\n"];
				cell.textLabel.text= [cell.textLabel.text stringByAppendingString:[weekDays objectAtIndex:i]];
				cell.textLabel.text= [cell.textLabel.text stringByAppendingString:@": \n"];
				
				for (HoursRange *hr in [[[eatHere times] timesForEachDay] objectAtIndex:i]) 
				{
					cell.textLabel.text= [cell.textLabel.text stringByAppendingString:hr.printBegin];
					cell.textLabel.text= [cell.textLabel.text stringByAppendingString:@"-"];
					cell.textLabel.text= [cell.textLabel.text stringByAppendingString:hr.printEnd];
					cell.textLabel.text= [cell.textLabel.text stringByAppendingString:@"\n"];
				}
			}
		}
		
		cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
		cell.textLabel.numberOfLines = 0;
		[cell.textLabel sizeToFit];
		[weekDays release];
		
	}

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  
{  
	/*	if (indexPath.section == 1) {
	 return 40.0;
	 }
	 */	
	if (indexPath.section == 0) return 140.0;
	if (indexPath.section == 2) return 70.0;
	if (indexPath.section == 3) return 800.0;
	
    return 40.0; //returns floating point which will be used for a cell row height at specified row index  
	
}  



/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.

	if (indexPath.section == 1)
	{
		MapViewController *mapVC = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
		mapVC.chosenName = eatHere.name; 
		// Pass the selected object to the new view controller.
		[self.navigationController pushViewController:mapVC animated:YES];
		[mapVC release];
	}
	
	if (indexPath.section == 2)	//open menu if restaurant is in coho
	{		
		if([cohoSet containsObject:eatHere.name]) 
		{
			UIWebView* menuView = [[UIWebView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 415.0)];
			
			UIViewController *menuViewController = [[UIViewController alloc] init];
			[menuViewController setTitle:@"Menu"];
			
			NSError *error = nil;
			NSURL *baseURL = [NSURL URLWithString: @"http://coffeehouse.ucdavis.edu/menu/pho"];
			NSString *htmlString = [NSString stringWithContentsOfURL:baseURL 
															encoding:NSUTF8StringEncoding error:&error];
			NSData *htmlData = [htmlString dataUsingEncoding:NSUnicodeStringEncoding];
			
			if (htmlData) {
				[menuView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:baseURL];
			}
			
			menuViewController.view = menuView;
			menuView.backgroundColor = [UIColor blackColor];
			
			[self.navigationController pushViewController:menuViewController animated:YES];
			[menuViewController release];
			[menuView release];

		}
		
		else 
		{
			//stuff goes here
			
			MenuDetailViewController *menuDVC = [[MenuDetailViewController alloc] initWithNibName:@"MenuDetailViewController" 
																						bundle:nil];
			menuDVC.eatName = eatHere.name;
			menuDVC.menuSet = eatHere.menus;
			
			// Pass the selected object to the new view controller.
			[self.navigationController pushViewController:menuDVC animated:YES];
			[menuDVC release];
			
		}

	}
	
	
	
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
	[cohoSet release];
	[eatHere release];
    [super dealloc];
}


@end

