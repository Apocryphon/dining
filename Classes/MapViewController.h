//
//  MapViewController.h
//  MapView
//
//  Created by Harvey Chan on 11/30/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController <MKMapViewDelegate>{
	IBOutlet MKMapView *mapView;
	NSDictionary *mapEatsDict;
	NSString *chosenName;
}

@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic, retain) NSDictionary *mapEatsDict;
@property (nonatomic, retain) NSString *chosenName;				// name of chosen location

@end

