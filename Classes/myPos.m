//
//  myPos.m
//  MapViewController.m
//  MapView
//
//  Created by Harvey Chan on 11/30/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import "MyPos.h"


@implementation MyPos

@synthesize coordinate, title, subtitle;

-(void)dealloc 
{
	[title release];
	[subtitle release];
	[super dealloc];
}

@end
