//
//  EateryDetailViewController.h
//  Dining
//
//  Created by Richard Yeh on 12/3/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Eatery.h"

@interface EateryDetailViewController : UITableViewController {
	Eatery *eatHere;
	NSSet *cohoSet;

}

@property (nonatomic, retain) Eatery *eatHere;					// current eatery
@property (nonatomic, retain) NSSet *cohoSet;					//this is a set of all coho restaurants


@end