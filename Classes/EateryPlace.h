//
//  EateryPlace.h
//  Dining
//
//  Created by Richard Yeh on 12/1/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <CoreData/CoreData.h>

#import "DataParser.h"

@interface EateryPlace : NSObject <UIApplicationDelegate> {	
	NSPersistentStoreCoordinator *persistentStoreCoordinator;
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;
	
}

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

//- (NSDictionary *) parseOutBlanks: (NSDictionary *)originalDict;
+ (EateryPlace *)sharedManager;



@end