//
//  DiningViewController.h
//  Dining
//
//  Created by Richard Yeh on 11/18/10.
//  Copyright 2010 UC Davis. All rights reserved.
//
//  Note: the MapView classes are instantiated directly from DVC's xib


#import <UIKit/UIKit.h>

@interface DiningViewController : UIViewController {
	UITabBarController *rootTabBarController;

}

@property (nonatomic, retain) IBOutlet UITabBarController *rootTabBarController;

- (IBAction)selectHome:(id)sender;
//- (IBAction)selectCaffeine:(id)sender;

@end
