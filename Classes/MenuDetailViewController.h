//
//  MenuDetailViewController.h
//  Dining
//
//  Created by Richard Yeh on 5/30/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Eatery.h"


@interface MenuDetailViewController : UITableViewController {
	NSString *_eatName;
	NSSet *_menuSet;
	NSArray *_sortedMenus;
}

@property (nonatomic, copy) NSString *eatName;					// current eatery name
@property (nonatomic, retain) NSSet *menuSet;						// set of Menus
@property (nonatomic, retain) NSArray *sortedMenus;

@end
