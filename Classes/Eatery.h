//
//  Eatery.h
//  Dining
//
//  Created by Richard Yeh on 5/11/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import <CoreData/CoreData.h>

@class Menu;

@interface Eatery :  NSManagedObject  
{
}

@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) id times;
@property (nonatomic, retain) NSString * website;
@property (nonatomic, retain) NSNumber * lon;
@property (nonatomic, retain) NSNumber * caffeine;
@property (nonatomic, retain) NSNumber * lat;
@property (nonatomic, retain) NSString * cuisine;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet* menus;

@end


@interface Eatery (CoreDataGeneratedAccessors)
- (void)addMenusObject:(Menu *)value;
- (void)removeMenusObject:(Menu *)value;
- (void)addMenus:(NSSet *)value;
- (void)removeMenus:(NSSet *)value;

@end

