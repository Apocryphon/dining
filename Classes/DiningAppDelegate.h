//
//  DiningAppDelegate.h
//  Dining
//
//  Created by Richard Yeh on 11/16/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataParser.h"

@class DiningViewController;

@interface DiningAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	DiningViewController *diningViewController;
	
	NSManagedObjectModel *managedObjectModel;
	NSManagedObjectContext *managedObjectContext;
	NSPersistentStoreCoordinator *persistentStoreCoordinator;
    DataParser *xmlParse;
	
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet DiningViewController *diningViewController;

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, retain) DataParser *xmlParse;

- (NSString *)applicationDocumentsDirectory;
-(void)testCoreData;


@end