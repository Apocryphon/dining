//
//  Menu.h
//  Dining
//
//  Created by Richard Yeh on 5/15/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import <CoreData/CoreData.h>

@class Eatery;
@class Item;

@interface Menu :  NSManagedObject  
{
}

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSNumber * day;
@property (nonatomic, retain) Eatery * eatery;
@property (nonatomic, retain) NSSet* items;

@end


@interface Menu (CoreDataGeneratedAccessors)
- (void)addItemsObject:(Item *)value;
- (void)removeItemsObject:(Item *)value;
- (void)addItems:(NSSet *)value;
- (void)removeItems:(NSSet *)value;

@end

