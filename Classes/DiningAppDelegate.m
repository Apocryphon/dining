//
//  DiningAppDelegate.m
//  Dining
//
//  Created by Richard Yeh on 11/16/10.
//  Copyright 2010 UC Davis. All rights reserved.
// blah!

#import "DiningAppDelegate.h"
#import "DiningViewController.h"
#import "Item.h"
#import "Eatery.h"
#import "Menu.h"
#import "EateryPlace.h"
#import "DataParser.h"

@implementation DiningAppDelegate

@synthesize xmlParse;
@synthesize window;
@synthesize diningViewController;

#pragma mark -
#pragma mark Core Data accessors

- (NSManagedObjectContext *) managedObjectContext {
	if (managedObjectContext != nil) {
		return managedObjectContext;
	}
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (coordinator != nil) {
		managedObjectContext = [[NSManagedObjectContext alloc] init];
		[managedObjectContext setPersistentStoreCoordinator: coordinator];
	}
	
	return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
	if (managedObjectModel != nil) {
		return managedObjectModel;
	}
	managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:nil] retain];
	
	return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	if (persistentStoreCoordinator != nil) {
		return persistentStoreCoordinator;
	}
	NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory]
											   stringByAppendingPathComponent: @"Dining.sqlite"]];
	NSError *error = nil;
	persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
								  initWithManagedObjectModel:[self managedObjectModel]];
	if(![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
												 configuration:nil URL:storeUrl options:nil error:&error]) {
		/*Error for store creation should be handled in here*/
	}
	
	return persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

-(void)testCoreData {
	NSError *error;
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	EateryPlace *eaterySingleton = [EateryPlace sharedManager];
	
	NSEntityDescription *entity = [NSEntityDescription 
								   entityForName:@"Eatery" inManagedObjectContext:[eaterySingleton managedObjectContext]];
	[fetchRequest setEntity:entity];
	NSArray *fetchedObjects = [[eaterySingleton managedObjectContext] executeFetchRequest:fetchRequest error:&error];
	for (NSManagedObject *info in fetchedObjects) {
		NSLog(@"Name root: %@", [info valueForKey:@"name"]);
		NSLog(@"Address root: %@", [info valueForKey:@"address"]);
		NSLog(@"Cuisine root: %@", [info valueForKey:@"cuisine"]);
		NSLog(@"Lat root: %@", [info valueForKey:@"lat"]);
		NSLog(@"Caffeine root: %@", [info valueForKey:@"caffeine"]);
	}
	
	[fetchRequest release];
}




#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
 
	EateryPlace *eaterySingleton = [EateryPlace sharedManager];
	[eaterySingleton managedObjectModel];
	
	//following block exemplifies multiple ways of retrieving .mom file path
	//Find targeted mom file in the Resources directory
	NSString *momPath = [[NSBundle mainBundle] pathForResource:@"Dining" ofType:@"mom"];
	NSLog(@"momd path 1: %@",momPath);
	
	
	//Find all of the mom and momd files in the Resources directory
	NSArray *momdArray = [[NSBundle mainBundle] pathsForResourcesOfType:@"mom"
															inDirectory:nil];
	for (NSString *momdPath in momdArray) { 
		NSString *resourceSubpath = [momdPath lastPathComponent]; 
		NSArray *array = [[NSBundle mainBundle]
						  pathsForResourcesOfType:@"mom"
						  inDirectory:resourceSubpath];
        NSLog(@"Array is: %@", array);
		NSLog(@"momd path 2: %@",momdPath);
		
	}
	
	NSURL *xmlURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Segundo" 
																		   ofType:@"xml"]];
	NSError *parseError = nil;
	
	self.xmlParse = [[DataParser alloc] initWithContext:self.managedObjectContext];
	[xmlParse parseXMLFileAtURL:xmlURL parseError:&parseError];

	[self testCoreData];

    [window addSubview:diningViewController.view];
    [window makeKeyAndVisible];
	
	
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}





#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
  [xmlParse release];
	[managedObjectContext release];
	[managedObjectModel release];
	[persistentStoreCoordinator release];
	
    [diningViewController release];
    [window release];
    [super dealloc];
}


@end
