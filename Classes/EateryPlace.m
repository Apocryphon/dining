//
//  EateryPlace.m
//  Dining
//
//  Created by Richard Yeh on 12/1/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import "EateryPlace.h"


@implementation EateryPlace

static EateryPlace *_sharedInstance = nil;

#pragma mark -
#pragma mark Singleton Methods

+ (EateryPlace *) sharedManager {
	if (_sharedInstance == nil) {
		_sharedInstance = [[super allocWithZone:NULL] init];
	}
	
	return _sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [[self sharedManager] retain];	
}


- (id)copyWithZone:(NSZone *)zone
{
    return self;	
}


- (id)retain
{
    return self;
}


- (NSUInteger)retainCount
{
    return NSUIntegerMax;  //denotes an object that cannot be released
}


- (void)release
{
    //do nothing
}


- (id)autorelease
{
    return self;
}

#pragma mark -
#pragma mark Helper Methods

/**
 Returns the path to the application's documents directory.
 */
- (NSString *)applicationDocumentsDirectory {
	
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

#pragma mark -
#pragma mark Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *) managedObjectContext {
	
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
	
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    return managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created by merging all of the models found in the application bundle.
 */
- (NSManagedObjectModel *)managedObjectModel {
	
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:nil] retain];
	
#pragma mark TODO change below line to search for your project's mom file
	//	NSString *path = [[NSBundle mainBundle] pathForResource:@"Dining" ofType:@"mom"];
    return managedObjectModel;
}


/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
	
	
	/// Need to handle what happens if database needs to be reinitialized
	
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] 
											   stringByAppendingPathComponent: @"Dining.sqlite"]];
	
	NSError *error;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error]) {
		// Handle the error.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
    }    
	NSLog(@"store path: %@", [storeUrl path]); //log path of store's location to console
	
    return persistentStoreCoordinator;
}


+ (EateryPlace *) sharedInstance
{
	if (!_sharedInstance)
	{
		_sharedInstance = [[EateryPlace alloc] init];
	}
	
	return _sharedInstance;
}


#pragma mark -
#pragma mark Memory management

-(void) dealloc
{
	[managedObjectContext release];
	[managedObjectModel release];
	[persistentStoreCoordinator release];
	[super dealloc];
	
}

@end
