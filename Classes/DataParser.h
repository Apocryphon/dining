//
//  DataParser.h
//  Dining
//
//  Created by Richard Yeh on 4/3/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Eatery.h"
#import "Menu.h"
#import "Item.h"
#import "HoursRange.h"
#import "Times.h"

@interface DataParser : NSObject <NSXMLParserDelegate>{
	
	NSManagedObjectContext *managedObjectContext;

	NSMutableString *currentValue;
	Eatery *currentEatery;
	Menu *currentMenu;
	Item *currentItem;
	BOOL inEatery;
	BOOL inMenu;
	BOOL inItem;
	BOOL inCoord;
	BOOL inTimes;
	
	NSMutableArray *weekCodesArray;
	NSString *currentOpen;
	NSString *currentClose;
	HoursRange *currentRange;
	Times *currentTime;
	NSMutableArray *currentWeekCodeRanges;
	NSMutableArray *allHourRangesOfEatery;			// array of currentWeekCodeRanges
	
	// may need instance of model object
}

@property (retain, nonatomic) NSMutableString *currentValue;
@property (assign) BOOL inEatery;
@property (assign) BOOL inMenu;
@property (assign) BOOL inItem;
@property (assign) BOOL inCoord;
@property (assign) BOOL inTimes;
@property (retain, nonatomic) NSMutableArray *weekCodesArray;
@property (copy, nonatomic) NSString *currentOpen;
@property (copy, nonatomic) NSString *currentClose;
@property (retain, nonatomic) HoursRange *currentRange;
@property (retain, nonatomic) Times *currentTime;
@property (retain, nonatomic) NSMutableArray *currentWeekCodeRanges;
@property (retain, nonatomic) NSMutableArray *allHourRangesOfEatery;
@property (retain, nonatomic) NSManagedObjectContext *managedObjectContext;

-(id) initWithContext: (NSManagedObjectContext *) managedObjContext;
- (void) trimString: (NSMutableString *) xmlString;
-(BOOL) parseXMLFileAtURL:(NSURL *)URL parseError:(NSError **)error;
-(void) emptyDataContext;


@end
