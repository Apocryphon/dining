//
//  MenuTableViewController.h
//  Dining
//
//  Created by Richard Yeh on 11/21/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EateryPlace.h"

@interface MenuTableViewController : UITableViewController <NSFetchedResultsControllerDelegate> {
///	NSDictionary *menuEatsDict;
///	NSMutableArray *eateryNames;
	BOOL caffeineBool;
    NSFetchedResultsController *_fetchedResultsController;
	
}

///@property (nonatomic, retain) NSDictionary *menuEatsDict;
///@property (nonatomic, retain) NSMutableArray *eateryNames;
@property (nonatomic) BOOL caffeineBool;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;


- (IBAction)selectCaffeine:(id)sender;

@end
