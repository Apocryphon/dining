//
//  RandomizedDetailViewController.h
//  Dining
//
//  Created by Richard Yeh on 12/10/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Eatery.h"


@interface RandomizedDetailViewController : UITableViewController {
	Eatery *eatHere;
	NSSet *cohoSet;
	
}

@property (nonatomic, retain) Eatery *eatHere;					// current eatery
@property (nonatomic, retain) NSSet *cohoSet;					//this is a set of all coho restaurants


@end
