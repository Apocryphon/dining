//
//  Times.h
//  Dining
//
//  Created by Richard Yeh on 4/11/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Times : NSObject <NSCoding> {
	NSMutableArray *timesForEachDay;
	NSMutableArray *weekCodesArray;
	NSMutableArray *allHourRangesOfEatery;

}
	
@property (nonatomic, retain) NSMutableArray *timesForEachDay;
@property (nonatomic, retain) NSMutableArray *weekCodesArray;
@property (nonatomic, retain) NSMutableArray *allHourRangesOfEatery;


- (id) initWithWeekCodesArray:(NSMutableArray *)wCA
	 AndAllHourRangesOfEatery:(NSMutableArray *)aHROE;
- (void) populateData;
- (void) displayData;

@end