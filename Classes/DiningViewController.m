//
//  DiningViewController.m
//  Dining
//
//  Created by Richard Yeh on 11/18/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import "DiningViewController.h"
#import "MenuTableViewController.h"

@implementation DiningViewController

@synthesize rootTabBarController;


- (void)selectHome:(id)sender {
	[[self parentViewController] dismissModalViewControllerAnimated:YES];
}

// originally selectCaffeine method here

/*
 // The designated initializer. Override to perform setup that is required before the view is loaded.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */



 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad {
	 [super viewDidLoad];
	 [self.view addSubview:self.rootTabBarController.view];
	 [self.rootTabBarController.view setFrame:self.view.frame];
 }

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.rootTabBarController viewWillAppear:animated];	// THIS IS VERY IMPORTANT see: http://goo.gl/bkzIW
	
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.rootTabBarController viewDidAppear:animated];     // THIS IS VERY IMPORTANT see: http://goo.gl/bkzIW
	
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.rootTabBarController viewWillDisappear:animated];	// THIS IS VERY IMPORTANT see: http://goo.gl/bkzIW
	
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.rootTabBarController viewDidDisappear:animated];	// THIS IS VERY IMPORTANT see: http://goo.gl/bkzIW
	
}



/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[rootTabBarController release];
    [super dealloc];
}


@end
