//
//  MapViewAppDelegate.h
//  MapView
//
//  Created by Harvey Chan on 11/30/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MapViewController;

@interface MapViewAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    MapViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet MapViewController *viewController;

@end

