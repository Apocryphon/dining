// 
//  Menu.m
//  Dining
//
//  Created by Richard Yeh on 5/15/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import "Menu.h"

#import "Eatery.h"
#import "Item.h"

@implementation Menu 

@dynamic name;
@dynamic desc;
@dynamic day;
@dynamic eatery;
@dynamic items;

@end
