//
//  myPos.h
//  MapView
//
//  Created by Harvey Chan on 11/30/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>

@interface MyPos : NSObject <MKAnnotation> 
{
	
	CLLocationCoordinate2D coordinate;
	NSString *title;
	NSString *subtitle;
}

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

@end
