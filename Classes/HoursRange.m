//
//  HoursRange.m
//  Dining
//
//  Created by Richard Yeh on 4/11/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import "HoursRange.h"


@implementation HoursRange
@synthesize beginTime, endTime;
@synthesize printBegin, printEnd;

-(id) initWithBeginTime:(NSString *)begin andEndTime:(NSString *)end
{
	self = [super init];
	if (self) {
		[self setBeginTimeWithString:begin];
		[self setEndTimeWithString:end];
	}
	
	return self;
}

// unfortunately, the times are wrong by default (offset to 1970) must be 
// converted to string from NSDate to be correct

-(void) setBeginTimeWithString: (NSString *)begin
{
	NSDateFormatter *formatFromXML = [[NSDateFormatter alloc] init];
	[formatFromXML setDateFormat:@"HH:mm"];
//	NSLog(@"begin: %@", begin);
	self.beginTime = [formatFromXML dateFromString:begin];
//	NSLog(@"beginTime: %@", beginTime);
//	NSLog(@"beginTime as displayed: %@", [formatFromXML stringFromDate:beginTime]);
	[formatFromXML release];
	
	NSDateFormatter * formatForDisplay = [[NSDateFormatter alloc] init];
	[formatForDisplay setDateFormat:@"h:mm a"];
	self.printBegin = [formatForDisplay stringFromDate:beginTime];
	[formatForDisplay release];
	
}

-(void) setEndTimeWithString: (NSString *)end
{
	NSDateFormatter *formatFromXML = [[NSDateFormatter alloc] init];
	[formatFromXML setDateFormat:@"HH:mm"];
//	NSLog(@"end: %@", end);
	endTime = [formatFromXML dateFromString:end];
//	NSLog(@"endTime: %@", endTime);
//	NSLog(@"endTime as displayed: %@", [formatFromXML stringFromDate:beginTime]);
	[formatFromXML release];
	
	NSDateFormatter * formatForDisplay = [[NSDateFormatter alloc] init];
	[formatForDisplay setDateFormat:@"h:mm a"];
	self.printEnd = [formatForDisplay stringFromDate:endTime];
	[formatForDisplay release];
	
}

-(void) printTimes
{
	NSLog(@"Opening time is: %@\n", printBegin);
	NSLog(@"Closing time is: %@\n", printEnd);
	
}

-(void) encodeWithCoder:(NSCoder *)aCoder
{
	[aCoder encodeObject:beginTime forKey: @"bT"];
	[aCoder encodeObject:endTime forKey: @"eT"];
	[aCoder encodeObject:printBegin forKey: @"pB"];
	[aCoder encodeObject:printEnd forKey: @"pE"];

}

-(id) initWithCoder:(NSCoder *)aDecoder
{
	if ((self = [self init])) 
	{
		self.beginTime = [[aDecoder decodeObjectForKey: @"bT"] retain];
		self.endTime = [[aDecoder decodeObjectForKey: @"eT"] retain];
		self.printBegin = [[aDecoder decodeObjectForKey: @"pB"] retain];
		self.printEnd = [[aDecoder decodeObjectForKey: @"pE"] retain];

	}
	
	return self;
}

-(void) dealloc
{
	[printEnd release];
	[printBegin release];
	[endTime release];
	[beginTime release];
	[super dealloc];
}

@end