//
//  HoursRange.h
//  Dining
//
//  Created by Richard Yeh on 4/11/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface HoursRange : NSObject <NSCoding> {
	NSDate *beginTime;
	NSDate *endTime;
	NSString *printBegin;
	NSString *printEnd;
}

@property (nonatomic, retain) NSDate * beginTime;
@property (nonatomic, retain) NSDate * endTime;
@property (nonatomic, retain) NSString *printBegin;
@property (nonatomic, retain) NSString *printEnd;

-(id) initWithBeginTime:(NSString *)begin andEndTime:(NSString *)end;
-(void) setBeginTimeWithString: (NSString *)begin;
-(void) setEndTimeWithString: (NSString *)end;
-(void) printTimes;

@end