//
//  MenuDetailViewController.m
//  Dining
//
//  Created by Richard Yeh on 5/30/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import "MenuDetailViewController.h"
#import "ItemDetailViewController.h"


@implementation MenuDetailViewController

@synthesize eatName = _eatName;
@synthesize menuSet = _menuSet;
@synthesize sortedMenus = _sortedMenus;

#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
	
	NSSortDescriptor *dayDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"day" ascending:YES] 
									   autorelease];
	
	NSSortDescriptor *nameDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES
																	 selector:@selector(localizedCaseInsensitiveCompare:)] autorelease];
	
	NSArray *sortDescriptors = [NSArray arrayWithObjects:dayDescriptor, nameDescriptor, nil];
	
	self.sortedMenus = [_menuSet sortedArrayUsingDescriptors:sortDescriptors];
	
	NSLog(@"%@", _sortedMenus);
	

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	
	self.navigationItem.title = [NSString stringWithFormat:@"Menus of %@", _eatName];
}

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [_menuSet count];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
	NSArray *weekDays = [[NSArray alloc] initWithObjects: @"Monday", @"Tuesday", @"Wednesday", 
						 @"Thursday", @"Friday", @"Saturday", @"Sunday", nil];

	Menu *menu = [_sortedMenus objectAtIndex:indexPath.row];
    cell.textLabel.text = [menu name];

	NSString *dayString = [[NSString alloc] initWithString:[weekDays objectAtIndex:[[menu day] intValue]]]; 
															// need to convert NSNumber day to int for index
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@", [menu desc], dayString];
	
	[dayString release];
	[weekDays release];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle 
									   reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
	
	[self configureCell:cell atIndexPath:indexPath];
	
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	//Get the selected menu.
	NSLog(@"index path: %d", indexPath.row);
	Menu *selectedMenu = [_sortedMenus objectAtIndex:indexPath.row];	
	NSLog(@"%@", selectedMenu);
	// Navigation logic may go here. Create and push another view controller.
    
    ItemDetailViewController *itemDVC = [[ItemDetailViewController alloc] initWithNibName:@"ItemDetailViewController" 
																				   bundle:nil];
	itemDVC.itemSet = [selectedMenu items];
	itemDVC.menuName = [selectedMenu name];
	
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:itemDVC animated:YES];
    [itemDVC release];
    
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
	[_sortedMenus release];
	[_menuSet release];
	[_eatName release];
    [super dealloc];
}


@end

