//
//  DataParser.m
//  Dining
//
//  Created by Richard Yeh on 4/3/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import "DataParser.h"
#import "CoreDataHelper.h"


@implementation DataParser

@synthesize managedObjectContext;
@synthesize currentValue;
@synthesize inEatery, inMenu, inItem, inCoord, inTimes;
@synthesize weekCodesArray;
@synthesize currentOpen, currentClose;
@synthesize currentTime;
@synthesize currentRange;
@synthesize currentWeekCodeRanges;
@synthesize allHourRangesOfEatery;

-(id) initWithContext: (NSManagedObjectContext *) managedObjContext
{
	self = [super init];
	[self setManagedObjectContext:managedObjContext];
	self.currentValue = [[NSMutableString alloc] init];
	self.inEatery = NO;
	self.inMenu = NO;
	self.inItem = NO;
	self.inCoord = NO;
	self.inTimes = NO;
	self.weekCodesArray = [[NSMutableArray alloc] init];
	self.currentOpen = [[NSString alloc] init];
	self.currentClose = [[NSString alloc] init];
	self.currentRange = [[HoursRange alloc] init];
	self.currentTime = [[Times alloc] init];
	self.currentWeekCodeRanges = [[NSMutableArray alloc] init];
	self.allHourRangesOfEatery = [[NSMutableArray alloc] init];
	
	return self;
}

- (void) trimString: (NSMutableString *) xmlString {
	NSString *temp = [xmlString stringByTrimmingCharactersInSet: 
					  [NSCharacterSet whitespaceAndNewlineCharacterSet]];
	[xmlString setString:temp];
}

-(BOOL) parseXMLFileAtURL:(NSURL *)URL parseError:(NSError **)error
{
	BOOL result = YES;
	
	NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:URL];
	// Set self as the delegate of parser so it can get the parser delegate methods callbacks.
	[parser setDelegate:self];
	
	[parser setShouldProcessNamespaces:NO];
    [parser setShouldReportNamespacePrefixes:NO];
    [parser setShouldResolveExternalEntities:NO];
    
    [parser parse];
    
    NSError *parseError = [parser parserError];
    if (parseError && error) {
        *error = parseError;
		result = NO;
    }
    
    [parser release];
	
	return result;
}

-(void) emptyDataContext
{
	// Delete core data contents
	BOOL resEat = [CoreDataHelper deleteAllObjectsForEntity:@"Eatery" andContext:managedObjectContext];
	if (resEat) { }
	
	BOOL resMen = [CoreDataHelper deleteAllObjectsForEntity:@"Menu" andContext:managedObjectContext];
	if (resMen) { }

	BOOL resItem = [CoreDataHelper deleteAllObjectsForEntity:@"Item" andContext:managedObjectContext];
	if (resItem) { }

	
	// Update the data model effectivly removing the objects we removed above.
	NSError *error;
	if (![managedObjectContext save:&error]) {
		
		// Handle the error.
		NSLog(@"%@", [error domain]);
	}
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName 
	attributes:(NSDictionary *)attributeDict
{
    if (qName) {
        elementName = qName;
    }
	
	// If it's the start of the XML, remove everything we've stored so far
	if([elementName isEqualToString:@"ucdm_dining_info"])
	{
		[self emptyDataContext];
		return;
	}
    
	// Create a new Eatery
    if ([elementName isEqualToString:@"eatery"]) 
	{
		inEatery = YES;
        currentEatery = (Eatery *)[NSEntityDescription insertNewObjectForEntityForName:@"Eatery" 
													inManagedObjectContext:managedObjectContext];
        return;				// Are these return statements really necessary in Eatery, Menu, Item, etc?
    }
	
	else if ([elementName isEqualToString:@"times"])
	{
		inTimes = YES;
		currentWeekCodeRanges = [[NSMutableArray alloc] init];
		weekCodesArray = [[NSMutableArray alloc] init];
		allHourRangesOfEatery = [[NSMutableArray alloc] init];
		return;
	}
	
	else if ([elementName isEqualToString:@"operating_days"])
	{
		return;
	}
	
	else if ([elementName isEqualToString:@"coordinates"])
	{
		inCoord = YES;
		return;
	}
	
	// Create a new Menu
	else if ([elementName isEqualToString:@"menu"])
	{
		inMenu = YES;
		currentMenu = (Menu *)[NSEntityDescription insertNewObjectForEntityForName:@"Menu" 
												inManagedObjectContext:managedObjectContext];
		
		// Add the Menu as a child to the current Eatery
		[currentEatery addMenusObject:currentMenu];
		NSLog(@"hey menu!");
		return;
    } 
	else if([elementName isEqualToString:@"item"])
	{
		inItem = YES;
		currentItem = (Item *)[NSEntityDescription insertNewObjectForEntityForName:@"Item" 
												inManagedObjectContext:managedObjectContext];

		[currentMenu addItemsObject:currentItem];
		[currentItem addMenuObject:currentMenu];
		NSLog(@"hey item!");

		return;
		
	}
}

// This method gets called for every character NSXMLParser finds.
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    // If currentValue doesn't exist, initialize and allocate
    if (!currentValue) {
		currentValue = [[NSMutableString alloc] init];
    }
    
    // Append the current character value to the running string
    // that is being parsed
    [currentValue appendString:string];
	[self trimString:currentValue];

}




- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName 
									namespaceURI:(NSString *)namespaceURI 
									qualifiedName:(NSString *)qName
{     
    if (qName) {
        elementName = qName;
	}
	
#pragma mark -
#pragma mark Set properties for Eatery

	if (inEatery)
	{
		if ([elementName isEqualToString:@"name"]) 
		{
			[currentEatery setName:currentValue];
			[currentValue release];
			currentValue = nil;
			return;
		}

		if ([elementName isEqualToString:@"address"]) 
		{
			[currentEatery setAddress:currentValue];
			[currentValue release];
			currentValue = nil;
			return;
		}
		
		if ([elementName isEqualToString:@"location"]) 
		{
			[currentEatery setLocation:currentValue];
			[currentValue release];
			currentValue = nil;
			return;
		}
		
		if ([elementName isEqualToString:@"caffeine"]) 
		{
			[currentEatery setCaffeine:[NSNumber numberWithBool:[currentValue boolValue]]];
			[currentValue release];
			currentValue = nil;
			return;
		}
		
		if ([elementName isEqualToString:@"cuisine"]) 
		{
			[currentEatery setCuisine:currentValue];
			[currentValue release];
			currentValue = nil;
			return;
		}
		
		if ([elementName isEqualToString:@"week_code"] && inTimes)
		{
			NSMutableString *days = (NSMutableString *)[currentValue stringByTrimmingCharactersInSet:
							  [[NSCharacterSet decimalDigitCharacterSet] invertedSet]];
			days = (NSMutableString *)[days stringByReplacingOccurrencesOfString:@"," withString:@""];
			[weekCodesArray addObject:days];
			[currentValue release];
			currentValue = nil;
		}
		

		if ([elementName isEqualToString:@"id"] && inTimes)
		{
		//	NSLog(@"id is: %@", currentValue);
			[currentValue release];
			currentValue = nil;
			
		}
		
		if ([elementName isEqualToString:@"open"] && inTimes)
		{
			currentOpen = currentValue;
			currentValue = nil;
		}
		
		if ([elementName isEqualToString:@"close"] && inTimes)
		{
			currentClose = currentValue;
			currentValue = nil;
			
		}

		if ([elementName isEqualToString:@"hours"] && inTimes)
		{
			currentRange = [[HoursRange alloc]initWithBeginTime:currentOpen 
								 andEndTime:currentClose];
			[currentWeekCodeRanges addObject:currentRange];

		}

		if ([elementName isEqualToString:@"operating_days"] && inTimes)
		{			
		// Pro-tip: cWCR MUST be cleared after every time, or it will store all times (because it is mutable)
		// Create a temporary array and copy current contents of cWCR into it
			[allHourRangesOfEatery addObject:[NSArray arrayWithArray:currentWeekCodeRanges]];
			[currentWeekCodeRanges removeAllObjects];


		}
		
		if ([elementName isEqualToString:@"times"] && inTimes)
		{
			currentTime = [[Times alloc] initWithWeekCodesArray:weekCodesArray
										   AndAllHourRangesOfEatery:allHourRangesOfEatery];
			NSLog(@"currentTime is:");
			[currentTime displayData];
			[currentEatery setTimes:currentTime];
			inTimes = NO;
			return;

		}

		if ([elementName isEqualToString:@"latitude"] && inCoord) 
		{
			[currentEatery setLat:[NSNumber numberWithDouble:[currentValue doubleValue]]];
			[currentValue release];
			currentValue = nil;
			return;
		}

		if ([elementName isEqualToString:@"longitude"] && inCoord) 
		{
			[currentEatery setLon:[NSNumber numberWithDouble:[currentValue doubleValue]]];
			[currentValue release];
			currentValue = nil;
			return;
		}	
		
		if ([elementName isEqualToString:@"coordinate"]) 
		{
			inCoord = NO;
			return;
		}
		
		if ([elementName isEqualToString:@"phone"]) 
		{
			[currentEatery setPhone:currentValue];
			[currentValue release];
			currentValue = nil;
			return;
		}
		
		if ([elementName isEqualToString:@"website"]) 
		{
			[currentEatery setWebsite:currentValue];
			[currentValue release];
			currentValue = nil;
			return;
		}
			
	}
	
	if ([elementName isEqualToString:@"eatery"]) 
	{
		// Sanity check
		if(currentEatery != nil)
		{
			NSError *error;
			
			// Store what we imported already
			if (![managedObjectContext save:&error]) {
				
				// Handle the error.
				NSLog(@"Eatery error: %@", [error domain]);
			}
		}
		
		NSLog(@"This eatery was: %@", currentEatery);
		inEatery = NO;
		return;
    }

#pragma mark -
#pragma mark Set properties for Menu
	
	if (inMenu && !inItem) 
	{
		if ([elementName isEqualToString:@"name"]) 
		{
			NSLog(@"inMenu is %d but inItem is %d", inMenu, inItem);
			[currentMenu setName:currentValue];
			NSLog(@"Menu name is: %@", currentValue);
			[currentValue release];
			currentValue = nil;
			return;
		}
		
		if ([elementName isEqualToString:@"description"]) 
		{
			[currentMenu setDesc:currentValue];
			NSLog(@"Menu description is: %@", currentValue);
			[currentValue release];
			currentValue = nil;
			return;
		}
		
		if ([elementName isEqualToString:@"day_of_week"]) 
		{
			NSNumberFormatter *tmpf = [[NSNumberFormatter alloc] init];
			[tmpf setNumberStyle:NSNumberFormatterDecimalStyle];

			[currentMenu setDay:[tmpf numberFromString:currentValue]];
//			NSLog(@"Day of this menu is: %@", [tmpf numberFromString:currentValue]);
			[currentValue release];
			currentValue = nil;
			[tmpf release];
			return;
		}
		
		// If we're at the end of a Menu. Save changes to object model
		if ([elementName isEqualToString:@"menu"]) 
		{		
			// Sanity check
			if(currentMenu != nil)
			{
				NSError *error;
				
				// Store what we imported already
				if (![managedObjectContext save:&error]) {
					
					// Handle the error.
					NSLog(@"Menu error: %@", [error domain]);
				}
			}
			NSLog(@"This menu was: %@", currentMenu);

			inMenu = NO;
			return;
		}
		
	}
	

	
#pragma mark -
#pragma mark Set properties for Item
	
	if (inItem && inMenu) 
	{
		if ([elementName isEqualToString:@"name"]) 
		{
//			NSLog(@"inMenu is %d", inMenu);
//			NSLog(@"inItem is %d", inItem);

			[currentItem setName:currentValue];
			[currentValue release];
			currentValue = nil;
			return;
		}
		
		if ([elementName isEqualToString:@"description"]) 
		{
			[currentItem setDesc:currentValue];
			[currentValue release];
			currentValue = nil;
			return;
		}
		
		if ([elementName isEqualToString:@"price"]) 
		{
			[currentItem setPrice:currentValue];
			[currentValue release];
			currentValue = nil;
			return;
		}
		
		// If we're at the end of an Item. Save changes to object model
		if ([elementName isEqualToString:@"item"]) 
		{
			// Sanity check
			if(currentItem != nil)
			{
				NSError *error;
				
				// Store what we imported already
				if (![managedObjectContext save:&error]) {
					
					// Handle the error.
					NSLog(@"Item error: %@", [error domain]);
				}
			}
			NSLog(@"This item was: %@", currentItem);

			inItem = NO;
			return;
		}
		
	}
	
	
}

#pragma mark -
#pragma mark Memory management

-(void)dealloc
{
	[allHourRangesOfEatery release];
	[currentWeekCodeRanges release];
	[currentRange release];
	[currentClose release];
	[currentOpen release];
	[weekCodesArray release];
	[currentItem release];
	[currentMenu release];
	[currentEatery release];
	[currentValue release];
	[managedObjectContext release];
	[super dealloc];
}


@end
