//
//  ItemDetailViewController.h
//  Dining
//
//  Created by Richard Yeh on 5/30/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"

@interface ItemDetailViewController : UITableViewController {
	NSString *_menuName;
	NSSet *_itemSet;
	NSArray *_sortedItems;
}

@property (nonatomic, copy) NSString *menuName;
@property (nonatomic, retain) NSSet *itemSet;
@property (nonatomic, retain) NSArray *sortedItems;


@end
