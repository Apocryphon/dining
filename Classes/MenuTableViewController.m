//
//  MenuTableViewController.m
//  Dining
//
//  Created by Richard Yeh on 11/21/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import "MenuTableViewController.h"
#import "EateryPlace.h"
#import "Eatery.h"
#import "EateryDetailViewController.h"

@implementation MenuTableViewController
///@synthesize menuEatsDict, eateryNames;
@synthesize caffeineBool;
@synthesize fetchedResultsController = _fetchedResultsController;


#pragma mark -
#pragma mark View lifecycle
- (void)selectCaffeine:(id)sender { //when user hits the caffeine button, this pops up a new MenuTableViewController showing only caffeine locations

	MenuTableViewController *menuTVC = [[MenuTableViewController alloc] 
										initWithNibName:@"MenuTableViewController" bundle:nil];
	menuTVC.caffeineBool=YES; //sets sorting to show only caffeine
	[self.navigationController pushViewController:menuTVC animated:YES];			// Pass the selected object to the new view controller.
	[menuTVC release];
}

- (NSFetchedResultsController *) fetchedResultsController {
	if (_fetchedResultsController != nil) {
		return _fetchedResultsController;
	}
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	EateryPlace *menuEats = [EateryPlace sharedManager];
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:@"Eatery" inManagedObjectContext:[menuEats managedObjectContext]];
	
	if (caffeineBool == YES) {																// Places with coffee!
		[fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"caffeine == 1"]];
	}
	
	[fetchRequest setEntity:entity];
	
	NSSortDescriptor *sort = [[NSSortDescriptor alloc]
							  initWithKey:@"cuisine" ascending:NO];
	
	[fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
		
	[fetchRequest setFetchBatchSize:20];
	
	NSFetchedResultsController *gotResultsController = [[NSFetchedResultsController alloc] 
														initWithFetchRequest:fetchRequest managedObjectContext:[menuEats managedObjectContext] 
														sectionNameKeyPath:nil
														cacheName:nil];				// cacheName must be set to nil for this to work!
	self.fetchedResultsController = gotResultsController;
	_fetchedResultsController.delegate = self;
	
	[sort release];
	[fetchRequest release];
	[gotResultsController release];
	
	return _fetchedResultsController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	
	//Initiate data source
    NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}	
	
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
}


/*
 - (void)viewWillAppear:(BOOL)animated {
 [super viewWillAppear:animated];
 }
 */
/*
 - (void)viewDidAppear:(BOOL)animated {
 [super viewDidAppear:animated];
 }
 */
/*
 - (void)viewWillDisappear:(BOOL)animated {
 [super viewWillDisappear:animated];
 }
 */
/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
 */
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    id <NSFetchedResultsSectionInfo> sectionInfo = 
	[[_fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Eatery *eatery = [_fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = eatery.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@, %@", 
								 eatery.cuisine, eatery.location];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] 
				 initWithStyle:UITableViewCellStyleSubtitle 
				 reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
	[self configureCell:cell atIndexPath:indexPath];
	
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	//Get the selected eatery's name.
	Eatery *eatery = [_fetchedResultsController objectAtIndexPath:indexPath];
	NSString *chosenName = eatery.name;
	NSLog(@"chosenName, or [_fetchedResultsController objectAtIndex:indexPath] is %@", chosenName);	

	// Navigation logic may go here. Create and push another view controller.
	
	EateryDetailViewController *eatDVC = [[EateryDetailViewController alloc] initWithNibName:@"EateryDetailViewController" bundle:nil];
	eatDVC.eatHere = eatery;
	
	// Pass the selected object to the new view controller.
	[self.navigationController pushViewController:eatDVC animated:YES];
	[eatDVC release];
	
}

#pragma mark -
#pragma mark NSFetchedResultsController delegate methods

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
	
    UITableView *tableView = self.tableView;
	
    switch(type) {
			
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
			
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
			
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
			
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            // Reloading the section inserts a new row and ensures that titles are updated appropriately.
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:newIndexPath.section] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
	
    switch(type) {
			
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
			
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}



#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
	self.fetchedResultsController = nil;

}


- (void)dealloc {
	
//	[menuEatsDict release];
	self.fetchedResultsController = nil;
///	[eateryNames release];
    [super dealloc];
}

#pragma mark -
#pragma mark failed parseOutBlanks function


@end

