//
//  Times.m
//  Dining
//
//  Created by Richard Yeh on 4/11/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import "Times.h"
#import "HoursRange.h"

@implementation Times

@synthesize timesForEachDay;
@synthesize weekCodesArray;
@synthesize allHourRangesOfEatery;

-(id) initWithWeekCodesArray:(NSMutableArray *)wCA
						AndAllHourRangesOfEatery:(NSMutableArray *)aHROE
{
	if (self = [super init]) 
	{
		self.timesForEachDay = [NSMutableArray arrayWithCapacity:7];	// create 2D array with blank arrays
		for (int i=0; i<7; i++)
		{
			[self.timesForEachDay addObject:[NSMutableArray arrayWithCapacity:1]];
		}
		
		self.weekCodesArray = [[NSMutableArray alloc] initWithArray:wCA];			// array of all possible wc's		
		self.allHourRangesOfEatery = [[NSMutableArray alloc] initWithArray:aHROE];	// array of all hr's from XML file
		[self populateData];
		
	}
	return self;
}


- (void) displayData
{
	for (int i=0; i<[timesForEachDay count]; i++) 
	{
		NSLog(@"This day is %d", i);

		for (HoursRange *hr in [timesForEachDay objectAtIndex:i])
		{
			[hr printTimes];
		}
	}
}

- (void) populateData
{
	
	NSDateFormatter *formatFromXML = [[NSDateFormatter alloc] init];
	[formatFromXML setDateFormat:@"HH:mm"];
	
//	int i = 0;
//	for (NSMutableArray *arr in allHourRangesOfEatery)
//	{
//		NSLog(@"This cWCR is: %d", i);
//		for(HoursRange *hr in arr)
//		{
//			NSLog(@"opening is: %@", [formatFromXML stringFromDate:[hr beginTime]]);
//			NSLog(@"closing is: %@", [formatFromXML stringFromDate:[hr endTime]]);
//		}
//		i++;
//	}
	
	for (int i=0; i < [weekCodesArray count]; i++)
	{
//		NSLog(@"This is weekcode: %@", [weekCodesArray objectAtIndex:i]);
		
		for (int j=0; j<7; j++) 
		{
//			NSLog(@"On this day: %d", j);
			
			// Week Code indicates this day has an hour range
			if ([[weekCodesArray objectAtIndex:i] characterAtIndex:j] == '1')	
			{

//				NSLog(@"Inserting into tFED at %d", j);
				
				for (HoursRange *hrFromXML in [allHourRangesOfEatery objectAtIndex:i])
				{
//					NSLog(@"bef: %@", [timesForEachDay objectAtIndex:j]);
					[[timesForEachDay objectAtIndex:j] addObject:hrFromXML];
//					NSLog(@"aft: %@", [timesForEachDay objectAtIndex:j]);
				}
				
				
/*				for (HoursRange *hrz in [allHourRangesOfEatery objectAtIndex:i])
				{
					NSLog(@"This range added to row.");
					NSLog(@"opening is: %@", [formatFromXML stringFromDate:[hrz beginTime]]);
					NSLog(@"closing is: %@", [formatFromXML stringFromDate:[hrz endTime]]);
				}
				NSLog(@"tFED is %@", timesForEachDay);
*/
			}
			
			else 
			{
//				NSLog(@"This weekcode has nothing on this day");
			}
			
		}

	}
	
}

- (void) encodeWithCoder:(NSCoder *)aCoder
{
	[aCoder encodeObject:allHourRangesOfEatery forKey: @"aHROE"];
	[aCoder encodeObject:weekCodesArray forKey: @"wCA"];
	[aCoder encodeObject:timesForEachDay forKey: @"tFED"];

}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if ((self = [self init])) 
	{
		self.allHourRangesOfEatery = [[aDecoder decodeObjectForKey: @"aHROE"] retain];
		self.weekCodesArray = [[aDecoder decodeObjectForKey: @"wCA"] retain];
		self.timesForEachDay = [[aDecoder decodeObjectForKey: @"tFED"] retain];
	}
	
	return self;
}

- (void) dealloc
{
	[allHourRangesOfEatery release];
	[weekCodesArray release];
	[timesForEachDay release];
	[super dealloc];
}

@end
