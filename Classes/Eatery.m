// 
//  Eatery.m
//  Dining
//
//  Created by Richard Yeh on 5/11/11.
//  Copyright 2011 UC Davis. All rights reserved.
//

#import "Eatery.h"

#import "Menu.h"

@implementation Eatery 

@dynamic phone;
@dynamic address;
@dynamic location;
@dynamic times;
@dynamic website;
@dynamic lon;
@dynamic caffeine;
@dynamic lat;
@dynamic cuisine;
@dynamic name;
@dynamic menus;

@end
