//
//  MapViewController.m
//  MapView
//
//  Created by Harvey Chan on 11/30/10.
//  Copyright 2010 UC Davis. All rights reserved.
//

#import "MapViewController.h"
#import "EateryDetailViewController.h"
#import "MyPos.h"
#import "EateryPlace.h"
#import "Eatery.h"

@implementation MapViewController

@synthesize mapView, mapEatsDict, chosenName;


- (void)dealloc 
{
	[mapView release];
    [super dealloc];
}


- (void)viewDidLoad 
{
    [super viewDidLoad];
	
	//Initiate data source
	NSError *error;
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	EateryPlace *mapEats = [EateryPlace sharedManager];
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:@"Eatery" inManagedObjectContext:[mapEats managedObjectContext]];
	[fetchRequest setEntity:entity];
	NSArray *fetchedObjects = [[mapEats managedObjectContext] executeFetchRequest:fetchRequest error:&error];
	NSMutableDictionary *tempDict = [NSMutableDictionary dictionaryWithCapacity:[fetchedObjects count]];
	
	for (Eatery *eatery in fetchedObjects) {
		[tempDict setObject:eatery forKey:eatery.name];
	}
	
	self.mapEatsDict = [NSDictionary dictionaryWithDictionary:tempDict];
	NSArray *dictKeys = [mapEatsDict allKeys];
	NSMutableArray *myPosArray = [NSMutableArray arrayWithCapacity:[dictKeys count]];
	
	MKCoordinateRegion regioncenter = { {0.0, 0.0 }, { 0.0, 0.0 } }; //set up new blank region
	if(chosenName == nil) regioncenter.center.latitude = 38.53882 ; //centers region on Silo by default
	if(chosenName == nil) regioncenter.center.longitude = -121.752869; //centers region on Silo by default
	regioncenter.span.longitudeDelta = 0.01f;
	regioncenter.span.latitudeDelta = 0.01f;	
	
	
	if(chosenName == nil){ //this checks if we didn't arrived here from the EateryDetailViewController
		for(int i=0;i<[mapEatsDict count];i++){
			Eatery *eatPlace = [mapEatsDict objectForKey:
								[dictKeys objectAtIndex:i]];
			
			MyPos *eatPlacePos = [[MyPos alloc] init];
			MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
			region.center.latitude = [eatPlace.lat floatValue];
			region.center.longitude = [eatPlace.lon floatValue];
			region.span.longitudeDelta = 0.01f;
			region.span.latitudeDelta = 0.01f;	
			eatPlacePos.title = eatPlace.name;
			eatPlacePos.subtitle = eatPlace.location;
			eatPlacePos.coordinate = region.center;
			[mapView addAnnotation:eatPlacePos];
			
			[myPosArray addObject:eatPlacePos]; //maybe not necessary
		}
	}
	else { //if we did enter from EateryDetailViewController, only lay down the chosen annotation
		Eatery *eatPlace = [mapEatsDict objectForKey:
							chosenName];
		
		MyPos *eatPlacePos = [[MyPos alloc] init];
		MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
		region.center.latitude = [eatPlace.lat floatValue];
		region.center.longitude = [eatPlace.lon floatValue];
		region.span.longitudeDelta = 0.01f;
		region.span.latitudeDelta = 0.01f;	
		eatPlacePos.title = eatPlace.name;
		eatPlacePos.subtitle = eatPlace.location;
		eatPlacePos.coordinate = region.center;
		[mapView addAnnotation:eatPlacePos];
		regioncenter.center.latitude = [eatPlace.lat floatValue];
		regioncenter.center.longitude = [eatPlace.lon floatValue];
	}
	
	[mapView setRegion:regioncenter animated:YES]; //center map on region of interest
	[mapView setDelegate:self];
	[mapView setMapType:MKMapTypeStandard];
	[mapView setZoomEnabled:YES];
	[mapView setScrollEnabled:YES];
	
	//Sample code for inserting pins
	/* 
	 MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
	 region.center.latitude = 38.53882 ;
	 region.center.longitude = -121.752869;
	 region.span.longitudeDelta = 0.01f;
	 region.span.latitudeDelta = 0.01f;	
	 [mapView setRegion:region animated:YES];
	 [mapView setDelegate:self];
	 
	 MyPos *ann = [[MyPos alloc] init];
	 ann.title = @"Silo";
	 ann.subtitle = @"Primary Dining Location";
	 ann.coordinate = region.center;
	 [mapView addAnnotation:ann];
	 
	 MKCoordinateRegion region1 = { {0.0, 0.0 }, { 0.0, 0.0 } };
	 region1.center.latitude = 38.53892 ;
	 region1.center.longitude = -121.752569;
	 region1.span.longitudeDelta = 0.01f;
	 region1.span.latitudeDelta = 0.01f;	
	 
	 MyPos *ann1 = [[MyPos alloc] init];
	 ann1.title = @"Other location";
	 ann1.subtitle = @"other place";
	 ann1.coordinate = region1.center;
	 [mapView addAnnotation:ann1];
	 */
	
	[fetchRequest release];
}


- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
	MKPinAnnotationView *pinView = nil;
	if(annotation != mapView.userLocation) 
	{
		static NSString *defaultPinID = @"dining.silo";
		pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
		if ( pinView == nil )
			pinView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID] autorelease];
		
		pinView.pinColor = MKPinAnnotationColorRed;
		pinView.canShowCallout = YES;
		pinView.animatesDrop = YES;
	}
	else
	{
		[mapView.userLocation setTitle:@"I am here"];
	}
	
	//these next two lines add the blue button to go to detail view
	UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
	pinView.rightCalloutAccessoryView = rightButton;
	
    return pinView;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
}


- (void)viewDidUnload 
{
	
}

- (void)mapView:(MKMapView *)mapView 
 annotationView:(MKAnnotationView *)view 
calloutAccessoryControlTapped:(UIControl *)control
{
	//Get the selected eatery's name.
	NSString *choiceName = view.annotation.title;
	
	// Create and push view controller of selected Eatery.
	EateryDetailViewController *eatDVC = [[EateryDetailViewController alloc] initWithNibName:@"EateryDetailViewController" bundle:nil];

	eatDVC.eatHere = [mapEatsDict objectForKey:choiceName];
	
	// Pass the selected object to the new view controller.
	[self.navigationController pushViewController:eatDVC animated:YES];
	[eatDVC release];
}


@end
